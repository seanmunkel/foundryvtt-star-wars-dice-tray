# Star Wars Dice Tray

This is a small module that makes it easier to quickly roll dice using Fantasy Flight Game's Star Wars RPG dice. It depends on the [Star Wars FFG](https://github.com/jaxxa/StarWarsFFG/tree/dev) system.

![preview](preview.png)


This was inspired by [Dice Calculator](https://gitlab.com/asacolips-projects/foundry-mods/foundry-vtt-dice-calculator)