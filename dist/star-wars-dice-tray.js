/**
 * This is your JavaScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your module, or remove it.
 * Author: Sean Munkel
 * Software License: Apache 2.0
 */

import { DicePoolFFG } from "/systems/starwarsffg/module/dice-pool-ffg.js";

import { registerSettings } from './module/settings.js';
import { preloadTemplates } from './module/preloadTemplates.js';


/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async function() {
	console.log('Star Wars Dice Tray | Initializing star-wars-dice-tray');
	// Register custom module settings
	registerSettings();
	
	// Preload Handlebars templates
	await preloadTemplates();
});

let activeDicePool = new DicePoolFFG();

Hooks.on("renderSidebarTab", async (app, html, data) => {
	let chatForm = html.find("#chat-form");
	if (chatForm.length == 0) {
		return;
	}
	const template = "modules/star-wars-dice-tray/templates/tray.html";
	const tray = $(await renderTemplate(template));
	const chatBox = chatForm.find("#chat-message");

	chatBox.keydown(event => {
		if (event.code === "Enter" || event.key === "Enter" || event.keycode === 13) {
			activeDicePool = new DicePoolFFG();
		}
	})

	const updateChatBox = () => {
		console.log("UPDATING", activeDicePool);
		chatBox.val(`/sw ${activeDicePool.renderDiceExpression()}`);
	}

	tray.find(".die").click(event => {
		const type = event.target.dataset.type;
		if (type == undefined) { 
			return 
		}
		activeDicePool[type]++;
		updateChatBox();
	});

	tray.find(".die").contextmenu(event => {
		const type = event.target.dataset.type;
		if (type == undefined) { 
			return 
		}
		if (activeDicePool[type] > 0) {
			activeDicePool[type]--;
		}
		updateChatBox();
	
	});

	tray.find("upgrade-ability").click(event => {
		activeDicePool.upgrade();
		updateChatBox();
	});

	tray.find("upgrade-difficulty").click(event => {
		activeDicePool.upgradeDifficulty();
		updateChatBox();
	})

	tray.find(".roll").click(event => {
		const spoofed = $.Event("keydown");
        spoofed.which = 13;
        spoofed.keycode = 13;
        spoofed.code = "Enter";
        spoofed.key = "Enter";
        chatBox.trigger(spoofed);
	});

	chatForm.after(tray);
});