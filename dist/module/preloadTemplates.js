export const preloadTemplates = async function() {
	const templatePaths = [
		"modules/star-wars-dice-tray/templates/tray.html",
	];

	return loadTemplates(templatePaths);
}
